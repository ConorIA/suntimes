FROM rstudio/plumber
MAINTAINER Conor Anderson <conor@conr.ca>

COPY plumber.R /api/plumber.R

#RUN echo 'deb http://deb.debian.org/debian bullseye main' > /etc/apt/sources.list

RUN apt-get update &&\
  apt-get install -y --no-install-recommends libgdal-dev libudunits2-dev &&\
  apt-get clean && rm -rf /tmp/* /var/lib/apt/lists/*

RUN cd /api &&\
  Rscript -e "install.packages(c('remotes', 'lutz', 'sf'))" &&\
  Rscript -e "remotes::install_github('ATFutures/calendar')" &&\
  Rscript -e "remotes::install_gitlab('ConorIA/claut')" &&\
  Rscript -e "source('https://gitlab.com/ConorIA/conjuntool/snippets/1788463/raw')" &&\
  rm -rf /tmp/*    

CMD ["/api/plumber.R"]
